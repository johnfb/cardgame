package no.ntnu.idatt2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Random;

/**
 * Represent a full deck of cards.
 * @author John Fredrik Bendvold
 * @version 1.0
 * @since 30.03.2022
 */
public class DeckOfCards {
    private ArrayList<PlayingCard> cards = new ArrayList<>();
    private final char[] suit = {'S', 'H', 'D', 'C'};

    public DeckOfCards() {
        for (char c : suit) {
            for (int j = 1; j < 14; j++) {
                cards.add(new PlayingCard(c, j));
            }
        }
    }

    public int getNumOfCards() {
        return cards.size();
    }

    /**
     * Method for getting random cards from the deck
     * @param n number of cards to deal
     * @return Arraylist with random cards from the deck
     */
    public ArrayList<PlayingCard> dealHand(int n) {
        if (n < 1 || n > 52) {
            throw new IllegalArgumentException("Must deal between 1 and 52 cards.");
        }

        ArrayList<PlayingCard> newCards = new ArrayList<>();
        Random rand = new Random();
        for (int i = 0; i < n; i++) {
            int cardPos = rand.nextInt(cards.size());
            newCards.add(cards.get(cardPos));
            cards.remove(cardPos);
        }

        return newCards;
    }
}
