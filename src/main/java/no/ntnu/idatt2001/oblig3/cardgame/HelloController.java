package no.ntnu.idatt2001.oblig3.cardgame;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class HelloController {
    private Hand hand;

    @FXML
    private Label queenOfSpadesLabel;
    @FXML
    private Label cardsOfHeartsLabel;
    @FXML
    private Label flushLabel;
    @FXML
    private Label cardDisplayLabel;
    @FXML
    private Label sumOfFacesLabel;

    @FXML
    public void checkHand(ActionEvent actionEvent) {
        sumOfFacesLabel.setText("Sum of faces: " + hand.getSumOfFaces());
        cardsOfHeartsLabel.setText("Cards of hearts: " + hand.getAllHearts());
        queenOfSpadesLabel.setText("Queen of spades: " + hand.containsQueenOfSpades());
        flushLabel.setText("Flush: " + hand.isFlush());
    }

    @FXML
    public void dealHand(ActionEvent actionEvent) {
        DeckOfCards deckOfCards = new DeckOfCards();
        hand = new Hand(deckOfCards.dealHand(5));

        cardDisplayLabel.setText(hand.toString());

        sumOfFacesLabel.setText("Sum of faces: ");
        cardsOfHeartsLabel.setText("Cards of hearts: ");
        queenOfSpadesLabel.setText("Queen of spades: ");
        flushLabel.setText("Flush: ");
    }
}