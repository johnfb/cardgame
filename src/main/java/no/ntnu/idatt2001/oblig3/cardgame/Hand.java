package no.ntnu.idatt2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Hand-class representing a players hand of cards
 * @author John Fredrik Bendvold
 * @version 1.0
 * @since 30.03.2022
 */
public class Hand {
    private ArrayList<PlayingCard> cards;

    /**
     * Constructor for a hand
     * @param cards the cards in the hand
     */
    public Hand(ArrayList<PlayingCard> cards) {
        this.cards = cards;
    }

    /**
     * Checks if all the cards have the same suit
     * @return true if flush
     */
    public boolean isFlush() {
        return cards.stream().map(PlayingCard::getSuit).distinct().count() == 1;
    }

    /**
     * Calculates the sum of all faces
     * @return int the sum of all faces
     */
    public int getSumOfFaces() {
        return cards.stream().mapToInt(PlayingCard::getFace).sum();
    }

    /**
     * Finds all hearts
     * @return all heart-cards
     */
    public String getAllHearts() {
        ArrayList<PlayingCard> hearts = cards.stream().filter(card -> card.getSuit() == 'H').collect(Collectors.toCollection(ArrayList::new));

        if (hearts.isEmpty()) {
            return "No Hearts";
        }

        return hearts.stream().map(card -> card.getAsString() + " ").collect(Collectors.joining());
    }

    /**
     * Checks if the hand contains queen of spades
     * @return true if it contains queen of spades
     */
    public boolean containsQueenOfSpades() {
        return cards.stream().anyMatch(card -> card.getAsString().equals("S12"));
    }

    @Override
    public String toString() {
        return cards.stream().map(card -> card.getAsString() + " ").collect(Collectors.joining());
    }
}
