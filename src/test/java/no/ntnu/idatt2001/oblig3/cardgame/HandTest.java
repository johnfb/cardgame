package no.ntnu.idatt2001.oblig3.cardgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;

public class HandTest {
    private Hand hand;
    private Hand hand2;

    @BeforeEach
    public void initializeHand() {
        ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('S', 5));
        cards.add(new PlayingCard('H', 10));
        cards.add(new PlayingCard('S', 5));
        cards.add(new PlayingCard('H', 12));
        cards.add(new PlayingCard('S', 2));

        hand = new Hand(cards);

        ArrayList<PlayingCard> cards2 = new ArrayList<>();
        cards2.add(new PlayingCard('S', 5));
        cards2.add(new PlayingCard('S', 12));
        cards2.add(new PlayingCard('S', 2));
        cards2.add(new PlayingCard('S', 3));
        cards2.add(new PlayingCard('S', 1));

        hand2 = new Hand(cards2);
    }

    @Test
    public void testSumOfFaces() {
        assertEquals(34, hand.getSumOfFaces());
        assertEquals(23, hand2.getSumOfFaces());
    }

    @Test
    public void testGetAllHearts() {
        assertEquals("H10 H12 ", hand.getAllHearts());
        assertEquals("No Hearts", hand2.getAllHearts());
    }

    @Test
    public void testContainsQueenOfSpades() {
        assertFalse(hand.containsQueenOfSpades());
        assertTrue(hand2.containsQueenOfSpades());
    }

    @Test
    public void testIsFlush() {
        assertFalse(hand.isFlush());
        assertTrue(hand2.isFlush());
    }
}
