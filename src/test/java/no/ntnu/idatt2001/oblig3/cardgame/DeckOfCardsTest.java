package no.ntnu.idatt2001.oblig3.cardgame;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {
    /**
     * Checks if all 52 cards get generated.
     */
    @Test
    public void deckOfCardsCreationTest() {
        DeckOfCards DOC = new DeckOfCards();
        assertEquals(52, DOC.getNumOfCards());
    }

    /**
     * Tests if cards get dealt.
     */
    @Test
    public void dealHandTest() {
        DeckOfCards DOC = new DeckOfCards();
        ArrayList<PlayingCard> cardsDealt = DOC.dealHand(5);

        assertEquals(47, DOC.getNumOfCards());

        assertEquals(5, cardsDealt.size());

        //Test with wrong input
        try{
            ArrayList<PlayingCard> cardsDealt2 = DOC.dealHand(53);
            fail("Method did not throw IllegalArgumentException as expected");
        }catch (Exception ex){
            assertEquals("Must deal between 1 and 52 cards.", ex.getMessage());
        }
    }
}
