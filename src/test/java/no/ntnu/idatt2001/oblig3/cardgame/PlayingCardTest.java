package no.ntnu.idatt2001.oblig3.cardgame;

import no.ntnu.idatt2001.oblig3.cardgame.PlayingCard;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PlayingCardTest {
    /**
     * Test the suit variable for the card.
     */
    @Test
    public void getSuitTest() {
        //Normal test
        PlayingCard card1 = new PlayingCard('S', 5);
        assertEquals(card1.getSuit(), 'S');

        //Test with lowercase input
        PlayingCard card2 = new PlayingCard('h', 8);
        assertEquals(card2.getSuit(), 'H');

        //Test with wrong input
        PlayingCard card3 = new PlayingCard('D', 10);
        assertNotEquals(card3.getSuit(), 'H');
    }

    /**
     * Test the face variable for the card.
     */
    @Test
    public void getFaceTest() {
        //Normal test
        PlayingCard card1 = new PlayingCard('S', 1);
        assertEquals(card1.getFace(), 1);

        //Normal test
        PlayingCard card2 = new PlayingCard('H', 8);
        assertEquals(card2.getFace(), 8);

        //Test with wrong input
        try{
            PlayingCard card3 = new PlayingCard('D', -500);
            fail("Method did not throw IllegalArgumentException as expected");
        }catch (Exception ex){
            assertEquals(ex.getMessage(), "The face value is invalid.");
        }

        //Test with wrong input
        try{
            PlayingCard card4 = new PlayingCard('D', 14);
            fail("Method did not throw IllegalArgumentException as expected");
        }catch (Exception ex){
            assertEquals(ex.getMessage(), "The face value is invalid.");
        }
    }

    /**
     * Test the getAsString-method
     */
    @Test
    public void getAsStringTest() {
        //Normal test
        PlayingCard card1 = new PlayingCard('S', 5);
        assertEquals(card1.getAsString(), "S5");

        //Test with lowercase input
        PlayingCard card2 = new PlayingCard('h', 8);
        assertEquals(card2.getAsString(), "H8");

        //Test with wrong input
        PlayingCard card3 = new PlayingCard('D', 10);
        assertNotEquals(card3.getAsString(), "d10");
    }
}
